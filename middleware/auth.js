const jwt = require('jsonwebtoken');


module.exports = {
  auth: async (req, res, next) => {
    try {
      const token = req.headers.authorization
        .split()[0]
        .replace(/Bearer\s/, '');
      const verify = await jwt.verify(token, process.env.JWT_SECRET);
      if (!verify) {
        return res.status(401).send();
      }
      next();
    } catch (e) {
      res.status(401).send();
    }
  },
};
