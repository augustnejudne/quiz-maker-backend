const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const accounts = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true,
    },
    tokens: [String]
  },
  {
    timestamps: true,
  }
);

accounts.methods.generateAuthToken = async function () {
  try {
    const account = this;
    const token = jwt.sign({ _id: account._id.toString() }, process.env.JWT_SECRET);
    account.tokens.push(token);
    account.save();
    return token;
  } catch (e) {
    return null;
  }
};

const Accounts = new mongoose.model('accounts', accounts);

module.exports = Accounts;

