const mongoose = require('mongoose');

const quizzes = new mongoose.Schema(
  {
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'accounts',
    },
    title: {
      type: String,
      required: true,
    },
    questions: [
      {
        question: String,
        answers: [{ type: String }],
        correctAnswer: Number,
      },
    ],
    icon: Number,
    colorScheme: Number,
    takers: {
      type: Number,
      default: 0
    }
  },
  {
    timestamps: true,
  },
);

const Quizzes = new mongoose.model('quizzes', quizzes);

module.exports = Quizzes;
