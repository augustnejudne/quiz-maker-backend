const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const users = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true,
    },
    tokens: [String]
  },
  {
    timestamps: true,
  }
);

users.methods.generateAuthToken = async function () {
  try {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET);
    user.tokens.push(token);
    user.save();
    return token;
  } catch (e) {
    return null;
  }
};

const Users = new mongoose.model('users', users);

module.exports = Users;
