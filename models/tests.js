const mongoose = require('mongoose');

const tests = new mongoose.Schema(
  {
    test: String
  },
  {
    timestamps: true,
  },
);

const Tests = new mongoose.model('tests', tests);

module.exports = Tests;

