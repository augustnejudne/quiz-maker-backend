const Accounts = require('models/accounts');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const saltRounds = 10;

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config();
}

/*========================================
user register
========================================*/
router.post('/register', async (req, res) => {
  try {
    const existingAcc = await Accounts.findOne({
      username: req.body.username,
    });
    if (existingAcc) return res.status(400).send();

    bcrypt.hash(req.body.password, saltRounds, async (err, hash) => {
      if (err) throw new Error();
      const body = {
        username: req.body.username,
        password: hash,
      };
      const newCreator = await new Accounts(body).save();
      const token = await newCreator.generateAuthToken();
      if (!token) throw new Error();
      res.send(token);
    });
  } catch (e) {
    res.status(500).send();
  }
});

/*========================================
user login
========================================*/
router.post('/login', async (req, res) => {
  try {
    const user = await Accounts.findOne({ username: req.body.username });
    if (!user) res.status(404).send();
    bcrypt.compare(req.body.password, user.password, async (err, result) => {
      if (err || !result) res.status(401).send();
      const token = await user.generateAuthToken();
      res.send(token);
    });
  } catch (e) {
    console.log('============================');
    console.log('Auth');
    console.log('e', e);
    console.log('============================');
    res.status(500).send();
  }
});

/*========================================
user logoout
========================================*/
router.get('/logout/:token', async (req, res) => {
  const token = req.params.token;
  try {
    const user = await Accounts.findOne({ tokens: token });
    user.tokens = user.tokens.filter((t) => t !== token);
    await user.save();
    res.send();
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
