const express = require('express');
const router = express.Router();
const Quizzes = require('models/quizzes');
const isEmpty = require('underscore').isEmpty;
const auth = require('middleware/auth').auth;

/*========================================
get all quizzes
========================================*/
router.get('/', async (req, res) => {
  const creator = req.query.creator;

  if (creator) {
    try {
      const quizzes = await Quizzes.find({ creator })
        .populate('creator', ['username'])
        .sort('-updatedAt');
      return res.send(quizzes);
    } catch (e) {
      return res.status(500).send();
    }
  }

  try {
    const quizzes = await Quizzes.find()
      .populate('creator', ['username'])
      .sort('-updatedAt');
    if (isEmpty(quizzes)) {
      return res.status(404).send();
    }
    return res.send(quizzes);
  } catch (e) {
    return res.status(500).send();
  }
});

/*========================================
get one quiz
========================================*/
router.get('/:quizId', async (req, res) => {
  const quizId = req.params.quizId;
  try {
    const quiz = await Quizzes.findById(quizId).populate('creator', [
      'username',
    ]);
    if (!quiz) {
      return res.status(404).send();
    }
    res.send(quiz);
  } catch (e) {
    res.status(500).send();
  }
});

/*========================================
edit one quiz
========================================*/
router.put('/:quizId', async (req, res) => {
  const quizId = req.params.quizId;

  // do this when just submitting a quiz
  const updateTakers = req.query.submit;
  if (updateTakers) {
    try {
      const quiz = await Quizzes.findById(quizId);
      quiz.takers += 1;
      await quiz.save();
      if (!quiz) {
        return res.status(404).send();
      }
      res.status(200).send();
    } catch (e) {
      res.status(500).send();
    }
    return;
  }

  // do this when updating the quiz
  const updateBody = req.body;
  if (!isEmpty(updateBody)) {
    try {
      const quiz = await Quizzes.findOneAndUpdate({ _id: quizId }, updateBody, {
        new: true,
      });
      if (!quiz) {
        return res.status(404).send();
      }
      res.status(200).send(quiz);
    } catch (e) {
      res.status(500).send();
    }
  }

  res.status(404).send();
});

/*========================================
create quiz
========================================*/
router.post('/', auth, async (req, res) => {
  try {
    const newQuiz = await new Quizzes(req.body).save();
    if (!newQuiz) {
      res.status(500).send();
    }
    res.status(200).send();
  } catch (e) {
    res.status(500).send();
  }
});

/*========================================
delete quiz
========================================*/
router.delete('/:quizId', auth, async (req, res) => {
  const quizId = req.params.quizId;
  try {
    await Quizzes.findOneAndDelete({ _id: quizId });
    res.status(200).send();
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
