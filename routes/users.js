const express = require('express');
const router = express.Router();
const Accounts = require('models/accounts');
const isEmpty = require('underscore').isEmpty;

/*========================================
get all users
========================================*/
router.get('/', async (req, res) => {
  try {
    const users = await Accounts.find().select(['displayName', 'picture']);
    if (isEmpty(users)) {
      return res.status(404).send();
    }
    return res.send(users);
  } catch (e) {
    return res.status(500).send();
  }
});

/*========================================
get one user
========================================*/
router.get('/profiles/:userId', async (req, res) => {
  const userId = req.params.userId;
  try {
    const user = await Accounts.findById(userId).select(['username']);
    if (!user) {
      return res.status(404).send();
    }
    return res.send(user);
  } catch (e) {
    return res.status(500).send();
  }
});

/*========================================
send user associated with token
========================================*/
router.get('/:token', async (req, res) => {
  const token = req.params.token;
  try {
    const user = await Accounts.findOne({ tokens: token }).select(['username']);
    if (!user) {
      return res.status(404).send();
    }
    res.send(user);
  } catch (e) {
    return res.status(500).send();
  }
});

module.exports = router;
