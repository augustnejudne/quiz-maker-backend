const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const morgan = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');
const quizzesRouter = require('./routes/quizzes');
const Tests = require('models/tests');

const app = express();

/*========================================
Mongoose
========================================*/
mongoose
  .connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log(`Connected to db: ${process.env.MONGODB_URI}`);
  })
  .catch((e) => {
    console.log(`Failed to connect to db: ${e}`);
  });

/*========================================
View engine
========================================*/
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

/*========================================
Middleware
========================================*/
app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
// CORS fix
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});
  }

  next();
});


/*========================================
Routes
========================================*/
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/quizzes', quizzesRouter);

app.get('/test', async (req, res) => {
  try {
    const test = new Tests({ test: 'test001' }).save();
    if (!test) res.json({ message: 'unable to create test001' });
    res.json({ message: 'test was created' });
  } catch (e) {
    res.json({ error: e });
  }
});

/*========================================
Listen
========================================*/
app.listen(process.env.PORT, () => {
  console.log(`App running on PORT: ${process.env.PORT}`);
});

module.exports = app;
